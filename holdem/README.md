holdem
======

* This package is an extension to the `decko` package.
* 2+2 poker hand evaluation algorithm is used for the best performance.
* The lookup table can be found under `holdem/data`
* Documentation can be found at [godoc.](http://godoc.org/github.com/tapir/decko/holdem)

License
=======

Original code was under 1-clause BSD license. With permission from the original
author, the license for this fork is changed to GPLv3. Its contents can be found
in the enclosed LICENSE file.
