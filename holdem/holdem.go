// Copyright (C) 2013 Jim Teeuwen, Coşku Baş
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

package holdem

import (
	"github.com/tapir/decko"
	"strconv"
	"strings"
)

// Hand represents the category of the hand and its rank.
type Hand struct {
	Category int
	Rank     int
}

// Set represents the set of 7 cards in a holdem game.
type Set [7]*decko.Card

// Hand categories.
const (
	Invalid = iota
	Highcard
	Pair
	TwoPair
	Trips
	Straight
	Flush
	FullHouse
	Quads
	StraightFlush
)

// Hand determines the highest kind of hand this set represents.
// It returns the hand category and rank.
func (s Set) Hand() *Hand {
	p := handRanks[53+s[0].Value]
	p = handRanks[p+int32(s[1].Value)]
	p = handRanks[p+int32(s[2].Value)]
	p = handRanks[p+int32(s[3].Value)]
	p = handRanks[p+int32(s[4].Value)]
	p = handRanks[p+int32(s[5].Value)]
	p = handRanks[p+int32(s[6].Value)]
	return &Hand{int(p >> 12), int(p & 0x00000FFF)}
}

// String returns a human readable representation of a hand - used by println.
func (h *Hand) String() string {
	switch h.Category {
	case Highcard:
		return "High card with rank " + strconv.Itoa(int(h.Rank))
	case Pair:
		return "Pair with rank " + strconv.Itoa(h.Rank)
	case TwoPair:
		return "Two pair with rank " + strconv.Itoa(h.Rank)
	case Trips:
		return "Three of a kind with rank " + strconv.Itoa(h.Rank)
	case Straight:
		return "Straight with rank " + strconv.Itoa(h.Rank)
	case Flush:
		return "Flush with rank " + strconv.Itoa(h.Rank)
	case FullHouse:
		return "Full house with rank " + strconv.Itoa(h.Rank)
	case Quads:
		return "Four of a kind with rank " + strconv.Itoa(h.Rank)
	case StraightFlush:
		return "Straight flush with rank " + strconv.Itoa(h.Rank)
	}

	panic("Invalid hand")
}

// String returns a human readable representation of a set - used by println.
func (s Set) String() string {
	list := make([]string, 7)

	for i, c := range s {
		list[i] = c.String()
	}

	return strings.Join(list, " ")
}
