// Copyright (C) 2013 Jim Teeuwen, Coşku Baş
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

package holdem

import (
	"encoding/binary"
	"os"
)

var handRanks [32487834]int32

// LoadLookupTable loads the lookup table for hand ranks from a file.
func LoadLookupTable(path string) {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}

	err = binary.Read(f, binary.LittleEndian, &handRanks)
	if err != nil {
		panic(err)
	}
}
