// Copyright (C) 2013 Jim Teeuwen, Coşku Baş
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

package decko

// Card represents a single card.
type Card struct {
	Value uint8
	inUse bool
}

// Card values named as Suit + Value.
const (
	C2 = iota + 1
	D2
	H2
	S2
	C3
	D3
	H3
	S3
	C4
	D4
	H4
	S4
	C5
	D5
	H5
	S5
	C6
	D6
	H6
	S6
	C7
	D7
	H7
	S7
	C8
	D8
	H8
	S8
	C9
	D9
	H9
	S9
	C10
	D10
	H10
	S10
	CJ
	DJ
	HJ
	SJ
	CQ
	DQ
	HQ
	SQ
	CK
	DK
	HK
	SK
	CA
	DA
	HA
	SA
)

func (c *Card) use() {
	c.inUse = true
}

func (c *Card) unuse() {
	c.inUse = false
}

// String returns a human readable representation of a card - used by println.
func (c *Card) String() string {
	switch c.Value {
	case C2:
		return "2♣"
	case D2:
		return "2♦"
	case H2:
		return "2♥"
	case S2:
		return "2♠"
	case C3:
		return "3♣"
	case D3:
		return "3♦"
	case H3:
		return "3♥"
	case S3:
		return "3♠"
	case C4:
		return "4♣"
	case D4:
		return "4♦"
	case H4:
		return "4♥"
	case S4:
		return "4♠"
	case C5:
		return "5♣"
	case D5:
		return "5♦"
	case H5:
		return "5♥"
	case S5:
		return "5♠"
	case C6:
		return "6♣"
	case D6:
		return "6♦"
	case H6:
		return "6♥"
	case S6:
		return "6♠"
	case C7:
		return "7♣"
	case D7:
		return "7♦"
	case H7:
		return "7♥"
	case S7:
		return "7♠"
	case C8:
		return "8♣"
	case D8:
		return "8♦"
	case H8:
		return "8♥"
	case S8:
		return "8♠"
	case C9:
		return "9♣"
	case D9:
		return "9♦"
	case H9:
		return "9♥"
	case S9:
		return "9♠"
	case C10:
		return "10♣"
	case D10:
		return "10♦"
	case H10:
		return "10♥"
	case S10:
		return "10♠"
	case CJ:
		return "J♣"
	case DJ:
		return "J♦"
	case HJ:
		return "J♥"
	case SJ:
		return "J♠"
	case CQ:
		return "Q♣"
	case DQ:
		return "Q♦"
	case HQ:
		return "Q♥"
	case SQ:
		return "Q♠"
	case CK:
		return "K♣"
	case DK:
		return "K♦"
	case HK:
		return "K♥"
	case SK:
		return "K♠"
	case CA:
		return "A♣"
	case DA:
		return "A♦"
	case HA:
		return "A♥"
	case SA:
		return "A♠"
	}

	panic("Undefined card")
}
