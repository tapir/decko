// Copyright (C) 2013 Jim Teeuwen, Coşku Baş
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

package decko

import (
	"crypto/rand"
	"math/big"
	"strings"
)

// Deck represents a single deck of cards.
type Deck struct {
	list [52]*Card
}

// New creates a new card deck.
func New() *Deck {
	return &Deck{
		[...]*Card{
			{HA, false},
			{H2, false},
			{H3, false},
			{H4, false},
			{H5, false},
			{H6, false},
			{H7, false},
			{H8, false},
			{H9, false},
			{H10, false},
			{HJ, false},
			{HQ, false},
			{HK, false},
			{DA, false},
			{D2, false},
			{D3, false},
			{D4, false},
			{D5, false},
			{D6, false},
			{D7, false},
			{D8, false},
			{D9, false},
			{D10, false},
			{DJ, false},
			{DQ, false},
			{DK, false},
			{CA, false},
			{C2, false},
			{C3, false},
			{C4, false},
			{C5, false},
			{C6, false},
			{C7, false},
			{C8, false},
			{C9, false},
			{C10, false},
			{CJ, false},
			{CQ, false},
			{CK, false},
			{SA, false},
			{S2, false},
			{S3, false},
			{S4, false},
			{S5, false},
			{S6, false},
			{S7, false},
			{S8, false},
			{S9, false},
			{S10, false},
			{SJ, false},
			{SQ, false},
			{SK, false},
		},
	}
}

// Pop pops the top available card from the deck and marks it as used.
// It returns nil when the deck is all used up.
func (d *Deck) Pop() *Card {
	for i := range d.list {
		if !d.list[i].inUse {
			d.list[i].use()
			return d.list[i]
		}
	}

	return nil
}

// Shuffle shuffles the cards in the deck using a
// cryptographic random number generator.
//
// The count parameter denotes how often the deck should be shuffled.
func (d *Deck) Shuffle(count int) {
	var (
		bi  *big.Int
		idx int64
	)

	max := big.NewInt(52)

	if count <= 0 {
		count = 1
	}

	for c := 0; c < count; c++ {
		for i := 0; i < 52; i++ {
			bi, _ = rand.Int(rand.Reader, max)
			idx = bi.Int64()
			d.list[i], d.list[idx] = d.list[idx], d.list[i]
		}
	}
}

// Reset resets the deck; makes all cards unused.
func (d *Deck) Reset() {
	for i := range d.list {
		d.list[i].unuse()
	}
}

// Count returns the amount of unused cards left in the deck.
func (d *Deck) Count() int {
	var n int

	for _, c := range d.list {
		if !c.inUse {
			n++
		}
	}

	return n
}

// String returns a human readable representation of a deck - used by println.
func (d *Deck) String() string {
	list := make([]string, 52)

	for i, c := range d.list {
		list[i] = c.String()

		if c.inUse {
			list[i] += "\tUsed"
		} else {
			list[i] += "\tNot Used"
		}
	}

	return strings.Join(list, "\n")
}
