decko
=====

* Decko is a fork of [Jim Teeuwen's](https://github.com/jteeuwen/deck) deck library to make it more suitable for the 2+2 poker hand evaluation algorithm.
* Deck exposes an API for a simple 52-card deck. It holds 52 indivual cards and exposes functions to manipulate them.
* Documentation can be found at [godoc.](http://godoc.org/github.com/tapir/decko)

License
=======

Original code was under 1-clause BSD license. With permission from the original
author, the license for this fork is changed to GPLv3. Its contents can be found
in the enclosed LICENSE file.
